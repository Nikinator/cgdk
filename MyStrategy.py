# coding: utf8
from model.Car import Car
from model.Game import Game
from model.Move import Move
from model.World import World
from model.TileType import TileType
from model.Direction import Direction
import math


include = lambda x0,x,x1: min(x0,x1) <= x <= max(x0,x1)
def alert(text):
    if 1: print text

class MyStrategy(object):
    _me = _world = _move = _game = _track = _track_reverse = _track_len = None

    def init(self, me, world, game, move):
        """Прокидывание изменяемых и неизменяемых(один раз)
        параметров игры в инстанс класса.
        :param Car me: Car, обновляемое
        :param World world: World, обновляемое
        :param Game game: Game, необновляемое
        :param Move move: Move, обновляемое
        """
        # always init
        self._me = me
        self._world = world
        self._move = move

        # once init
        if not getattr(self, '_init', False):
            # get_range = lambda a,b: xrange(a, b, 1 if a < b else -1)
            alert('map:')
            for line_y in zip(*world.tiles_x_y):
                alert(map(lambda t: TT._verbose_name[t],line_y))
            waypoints = map(tuple, world.waypoints)
            if world.map_name == 'map06':
                waypoints.insert(-1, (8,12))
                waypoints.append((14,12))
            alert('waypionts:\n {}'.format(waypoints))

            get_first = lambda v: v[0]

            # init value
            p0, dir_in, track = waypoints.pop(0), world.starting_direction, []
            waypoints.append(p0)
            for p1 in waypoints:
                node = self.get_end_node(Node(p0, dir_in), p1)
                if node:

                    if isinstance(node, list):
                        res = [n.get_branch(get_first)[:-1] for n in node]
                        # в track хранить наше дерево, и val = (p, dir_in, type_tile)
                    track.extend(node.get_branch(get_first)[:-1])
                    # node.print_tree()
                    p0, dir_in = node.val
                else:
                    alert(('Error: return None ', p0, dir_in, p1))
            self._track = track
            self._track_reverse = self._track[::-1]
            self._track_len = len(self._track)
            # противники (все игроки исключая членов команды)
            self._opponents = filter(lambda c: not c.teammate, world.cars)
            alert('opponents ids:\n{}'.format(map(lambda c: c.player_id, self._opponents)))
            self._game = game
            self._corner_tile_offset = 0.25 * game.track_tile_size #0.25

            alert('track(len={}):\n{}'.format(len(self._track), self._track))
            self._init = True  # for once init

    def bad_point(self, p0, p, di, p1):
        # если не можем зайти в тайл - плохая точка
        try: TT.get_directs_out(self.get_tile(*p), di)
        except AssertionError, e: return True  # e

        (x0, y0), (x, y), (x1, y1) = p0, p, p1
        dx, dy = abs(x1 - x0), abs(y1 - y0)

        if dx == dy:
            # если только обе координаты не лежат между p0,p1 - плохая точка
            if not (include(x0, x, x1) or include(y0, y, y1)): return True
        else:
            # если хоть одна координата не лежит между p0,p1 - плохая точка
            if not (include(x0, x, x1) and include(y0, y, y1)): return True

        # если оказались здесь - нормальная точка
        return False

    def get_end_node(self, node, p1):
        """Рекурсивный обход дерева, возвращается узел p1,
        по которому можно восстановить путь от p0.
        :param Node node: Node(p0, dir_in)
        :param tuple p1: tuple(x, y)
        :return Node: end_node
        """
        p0, di = node.val
        # уже добрались до желаемой точки - возвращаем узел
        if p0 == p1: return node

        # возможные выходы из тайла при указанном входе
        dirs_out = TT.get_directs_out(self.get_tile(*p0), di)

        # отбирем только "хорошие" перемещения и
        # устанавливаем их дочерними узлами для node
        good_ways = []
        for do in dirs_out:
            p_n, di_n = D.get_next_point(p0, do), do
            # alert((self.bad_point(p0, p_n, di_n, p1),p0, p_n, di_n, p1))
            if self.bad_point(p0, p_n, di_n, p1): continue
            good_ways.append((p_n, di_n))
        node.childs = [Node(*way) for way in good_ways]

        # Рекурсивно(в глубину) обходим дочение узлы,
        # получив списов возможных листьевых узлов,
        # которые позволили добраться до точки p1.
        end_nodes = filter(None, [
            self.get_end_node(child, p1) for child in node.childs])
        # print 11111, end_nodes
        # Далее лишь возвращаем кратчайший из них или
        # None - не получилось добраться конечной точки.
        if not end_nodes:
            end_node = None
        elif len(end_nodes) == 1:
            end_node = end_nodes[0]
        else:
            # в списке будет всего 2-3 элемента, поэтому можно
            # в качестве ключа для сортировки указать .length
            print 11111, map(lambda n: (n, n.length) ,end_nodes)
            end_node = sorted(end_nodes, key=lambda n: n.length)[0]

        return end_node

    def log(self):
        tiles = self.get_cur_tuple_tiles()
        if self._me.speed_x or self._me.speed_y:
            alert( ''.join((
                self._world.tick,
                '|1|', self.get_cur_tile(), self.get_cur_xi_yi(),
                '|2|', tiles, TT._rotation.get(tiles, 'unknown ' + str(tiles)),
                '|3|', (self._me.next_waypoint_x, self._me.next_waypoint_y),
                self.get_next_wp_xi_yi())))

    def get_xi_yi(self, x, y):
        return int(x / self._game.track_tile_size), int(y / self._game.track_tile_size)

    def get_cur_xi_yi(self):
        return self.get_xi_yi(self._me.x, self._me.y)

    def get_next_wp_xi_yi(self):
        return self._me.next_waypoint_x, self._me.next_waypoint_y

    def get_next_wp_x_y(self):
        return map(lambda v: (v + 0.5) * self._game.track_tile_size, [
            self._me.next_waypoint_x, self._me.next_waypoint_y])

    def get_tile(self, xi, yi):
        """Возвращает тип тайла:
        get_tile(0, 0) -  по xi,yi
        get_tile(0) - по i индексу из _track
        """
        return self._world.tiles_x_y[xi][yi]

    def get_cur_tile(self):
        return self.get_tile(*self.get_cur_xi_yi())

    def get_next_wp_tile(self):
        # return self._world.tiles_x_y[self._me.next_waypoint_x][self._me.next_waypoint_y]
        return self.get_tile(*self.get_next_wp_xi_yi())

    def get_cur_tuple_tiles(self, count=3):
        """Возвращает колтеж длины @count ближайших тайлов, начиная с текцщего
        :param int count:
        :return tuple: (1, 6, 2)
        """
        cur_xi_yi = self.get_cur_xi_yi()

        # определяем индекс текущего тайла (сложности возникают если есть перекрестки)
        if self._track.count(cur_xi_yi) == 1:
            cur_i = self._track.index(cur_xi_yi)
        else:
            # alert( u'Перекресток вашу мать!!!!')
            # если >1, то находим индекс текущего тайла по следующему(ключевому)
            r_next_i = self._track_reverse.index(self.get_next_wp_xi_yi())
            cur_i = self._track_len - 1 - self._track_reverse.index(cur_xi_yi, r_next_i)

        # эмулируем замкнутость трассы, если число кругов >1. может это не всегда нужно!!!
        last_i = cur_i + count
        if last_i <= self._track_len or self._game.lap_count == 1:
            tuple_x, tuple_y = zip(*self._track[cur_i:cur_i + count])
        else:
            # alert( u'Замыкаем круг, на!')
            # alert( (cur_i, self._track_len), (0, last_i - self._track_len))
            tuple_x, tuple_y = zip(*(self._track[cur_i:self._track_len] +
                                   self._track[:last_i - self._track_len]))
        return tuple(map(self.get_tile, tuple_x, tuple_y))

    def calc_power(self):
        """Расчет и применение педали газа"""
        power = .95  # me.engine_power = 1
        next_tile = self.get_next_wp_tile()
        # dists_to_cars = map(self._me.get_distance_to_unit, self._opponents)
        if next_tile in [TT.LTC, TT.RTC, TT.LBC, TT.RBC, TT.TT]:
            power = 0.75
        # if filter(lambda v: v < 50, dists_to_cars) and next_tile != TT.TH:
        #     power = 0.5

        self._move.engine_power = power

    def calc_rotation(self):
        """Расчет, применение и возвращение угла поворота,
        необходимого чтобы выровнять траекторию до следующего ключегого тайла.
        :return float: угол поворота
        """
        p_cur = self.get_cur_xi_yi()
        p_next_ind = self._track.index(p_cur)+1
        # ближайший
        p_next = self._track[p_next_ind] if p_next_ind < len(self._track) else self._track[0]
        next_wp_x, next_wp_y = map(lambda v: (v + 0.5) * self._game.track_tile_size, p_next) #self.get_next_wp_x_y()
        next_wp_tile = self.get_tile(*p_next)#self.get_next_wp_tile()

        if next_wp_tile == TT.LTC:
            next_wp_x += self._corner_tile_offset
            next_wp_y += self._corner_tile_offset
        elif next_wp_tile == TT.RTC:
            next_wp_x -= self._corner_tile_offset
            next_wp_y += self._corner_tile_offset
        elif next_wp_tile == TT.LBC:
            next_wp_x += self._corner_tile_offset
            next_wp_y -= self._corner_tile_offset
        elif next_wp_tile == TT.RBC:
            next_wp_x -= self._corner_tile_offset
            next_wp_y -= self._corner_tile_offset
        # elif next_wp_tile in [TT.TT, TT.BT, TT.LT, TT.RT]:
            # print next_wp_x, next_wp_y

        angle_to_waypoint = self._me.get_angle_to(next_wp_x, next_wp_y)
        # print angle_to_waypoint, angle_to_waypoint * 32.0 / math.pi
        self._move.wheel_turn = angle_to_waypoint * 32.0 / math.pi #32.0
        return angle_to_waypoint

    def calc_braking(self, angle_to_waypoint):
        """Расчет и применение торможения
        :param angleToWaypoint: угол
        """
        speed_module = math.hypot(self._me.speed_x, self._me.speed_y)

        if (math.pow(speed_module, 2) * abs(angle_to_waypoint)) > (math.pow(2.5, 2) * math.pi):
            self._move.brake = True

        # dists_to_cars = map(self._me.get_distance_to_unit, self._opponents)
        # rydom = bool(filter(lambda v: v < 50,dists_to_cars))
        #
        # if TT.TH in self.get_cur_tuple_tiles(2) and rydom and self._world.tick % 5 == 0:
        #     alert( 'brake')
        #     self._move.brake = True

    def calc_bonuses(self):
        """Определение и использование бонусов"""
        next_tile = self.get_next_wp_tile()
        if next_tile in [TT.LTC, TT.RTC, TT.LBC, TT.RBC]:
            self._move.spill_oil = True
        else:
            self._move.use_nitro = True

        dists_to_cars = map(self._me.get_distance_to_unit, self._opponents)

        # TODO: условие что есть кто то впереди или через get_angle_to_unit
        if dists_to_cars:
            self._move.throw_projectile = True

    def move(self, me, world, game, move):
        """Метод вызываемый в Runner, как обработчик на стороне клиента
        :param Car me: Car
        :param World world: World
        :param Game game: Game
        :param Move move: Move
        """
        self.init(me, world, game, move)

        self.calc_power()
        angle_to_waypoint = self.calc_rotation()
        self.calc_braking(angle_to_waypoint)
        self.calc_bonuses()
        # self.log()


class D(Direction):
    L = Direction.LEFT
    R = Direction.RIGHT
    U = Direction.UP
    D = Direction.DOWN
    _revers = {L: R, R: L, U: D, D: U}

    @classmethod #OK
    def get_rev(cls, dir):
        """обратное направление"""
        return cls._revers[dir]

    @classmethod #OK
    def get_next_point(cls, p, direct):
        x, y = p
        if direct == cls.UP: y -= 1
        elif direct == cls.DOWN: y += 1
        elif direct == cls.LEFT: x -= 1
        elif direct == cls.RIGHT: x += 1
        else: assert 0, u"Указано не существующее направление '{}'".format(direct)
        return (x, y)


class TT(TileType):
    """переопределенные типы тайлов"""
    E = TileType.EMPTY
    V = TileType.VERTICAL
    H = TileType.HORIZONTAL
    LTC = TileType.LEFT_TOP_CORNER
    RTC = TileType.RIGHT_TOP_CORNER
    LBC = TileType.LEFT_BOTTOM_CORNER
    RBC = TileType.RIGHT_BOTTOM_CORNER
    LT = TileType.LEFT_HEADED_T
    RT = TileType.RIGHT_HEADED_T
    TT = TileType.TOP_HEADED_T
    BT = TileType.BOTTOM_HEADED_T
    C = TileType.CROSSROADS
    U = TileType.UNKNOWN

    _verbose_name = {
        E: 'e', V: 'v', H: 'h', C: 'c', U: 'u',
        LTC: 'ltc', RTC: 'rtc', LBC: 'lbc', RBC: 'rbc',
        LT: 'lt', RT: 'rt', TT: 'tt', BT: 'bt',
    }

    _directions = {
        E: [],                          # ▢
        V: [D.UP, D.DOWN],                  # | ║
        H: [D.LEFT, D.RIGHT],               # ⎻ ═
        LTC: {D.LEFT: [D.DOWN], D.UP: [D.RIGHT]},   # ╔
        RTC: {D.RIGHT: [D.DOWN], D.UP: [D.LEFT]},   #  ╗
        LBC: {D.LEFT: [D.UP], D.DOWN: [D.RIGHT]},   # ╚
        RBC: {D.RIGHT: [D.UP], D.DOWN: [D.LEFT]},   #  ╝
        LT: [D.LEFT, D.UP, D.DOWN],         # ⊣  ╣
        RT: [D.RIGHT, D.UP, D.DOWN],        # Ꮀ ╠
        TT: [D.UP, D.LEFT, D.RIGHT],        # ᚆ  ╩
        BT: [D.DOWN, D.LEFT, D.RIGHT],      # T ╦
        C: [D.UP, D.DOWN, D.LEFT, D.RIGHT], # +  ╬
        U: [],                          # ▢
    }

    _not_direct_in = "У типа тайла '{}' нет входящего направления '{}'."
    @classmethod #OK
    def get_directs_out(cls, tile_type, direct_in):
        """print TT.get_directs_out(TT.LBC, D.LEFT)"""
        outs = cls._directions[tile_type]
        if outs:
            if isinstance(outs, list):
                assert D.get_rev(direct_in) in outs, (
                    cls._not_direct_in.format(tile_type, direct_in))
                outs = filter(lambda d: d != D.get_rev(direct_in), outs)
            elif isinstance(outs, dict):
                assert direct_in in outs, (
                    cls._not_direct_in.format(tile_type, direct_in))
                outs = outs[direct_in]
        return outs

    @classmethod
    def get_filter_directs_in(cls, tile_type, directs_in):
        """отфильтрует недоступные направления вхождения для данного типа тайла"""
        return filter(lambda di: di in cls._directions[tile_type], directs_in)

    _rotation = {
        (V, V, LTC): 'left_begin',
        (H, H, RTC): 'left_begin',
        (V, V, RBC): 'left_begin',
        (H, H, LBC): 'left_begin',

        (H, H, LTC): 'right_begin',
        (V, V, RTC): 'right_begin',
        (H, H, RBC): 'right_begin',
        (V, V, LBC): 'right_begin',
    }


class Node(object):
    """Api дерева со множеством потомков и ссылкой на родителя.
    root = Node('a'); root.childs = [Node('b'), Node('c')]
    print root.childs[0].get_branch()
    """
    def __init__(self, *_val):
        self._val, self._parent, self._childs = _val, None, []

    @property
    def childs(self): return self._childs
    @childs.setter
    def childs(self, value):
        assert isinstance(value, list), 'Must be list!'
        self._childs = value
        for c in self._childs: c.parent = self

    @property
    def parent(self):
        """:return: Node"""
        return self._parent
    @parent.setter
    def parent(self, value):
        """:param Node value: Node"""
        assert isinstance(value, self.__class__), 'Must be Node!'
        self._parent = value

    @property
    def val(self):
        return self._val if len(self._val) > 1 else self._val[0]
    @val.setter
    def val(self, *value): self._val = value

    @property
    def is_leaf(self):
        """является листьевым узлом"""
        return not len(self._childs)

    # методы использующие обход вершин вверх или вниз:
    def print_tree(self):
        """печатает узел и его потоков в виде дерева"""
        print '    ' * (self.length - 1) + str(self)
        for ch in self._childs:
            ch.print_tree()

    @property
    def length(self):
        """длина ветки оn корневого элемента"""
        return (self._parent.length + 1) if self._parent else 1

    def get_branch(self, func_val=None):
        """список val'ов на ветке от корневого элемента"""
        val = func_val(self._val) if func_val else self.val
        if self._parent:
            res = self._parent.get_branch(func_val)
            res.append(val)
            return res
        return [val]

    def is_contain(self, val, func_val=None):
        """
        :param Node|Any val: Node|Any
        :param function func_val: function
        """
        if isinstance(val, self.__class__): val = val.val
        is_found = (func_val(self._val) if func_val else self.val) == val
        if not is_found and self._parent:
            is_found = self._parent.is_contain(val, func_val)
        return is_found

    def __str__(self): return "Node('{}', '{}')".format(
        self.val, 'root' if self.parent is None else (
            'leaf' if self.is_leaf else 'node'))
    def __repr__(self): return str(self)