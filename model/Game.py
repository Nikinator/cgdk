# coding: utf8
from math import *


class Game:
    def __init__(self, random_seed, tick_count, world_width, world_height, track_tile_size, track_tile_margin,
                 lap_count, lap_tick_count, initial_freeze_duration_ticks, burning_time_duration_factor,
                 finish_track_scores, finish_lap_score, lap_waypoints_summary_score_factor, car_damage_score_factor,
                 car_elimination_score, car_width, car_height, car_engine_power_change_per_tick,
                 car_wheel_turn_change_per_tick, car_angular_speed_factor, car_movement_air_friction_factor,
                 car_rotation_air_friction_factor, car_lengthwise_movement_friction_factor,
                 car_crosswise_movement_friction_factor, car_rotation_friction_factor, throw_projectile_cooldown_ticks,
                 use_nitro_cooldown_ticks, spill_oil_cooldown_ticks, nitro_engine_power_factor, nitro_duration_ticks,
                 car_reactivation_time_ticks, buggy_mass, buggy_engine_forward_power, buggy_engine_rear_power,
                 jeep_mass, jeep_engine_forward_power, jeep_engine_rear_power, bonus_size, bonus_mass,
                 pure_score_amount, washer_radius, washer_mass, washer_initial_speed, washer_damage, side_washer_angle,
                 tire_radius, tire_mass, tire_initial_speed, tire_damage_factor, tire_disappear_speed_factor,
                 oil_slick_initial_range, oil_slick_radius, oil_slick_lifetime, max_oiled_state_duration_ticks):
        self.bonus_mass = bonus_mass  # =100.0
        self.bonus_size = bonus_size  # =70.0
        self.pure_score_amount = pure_score_amount  # =100
        #   Возвращает количество баллов, мгновенно получаемых игроком, кодемобиль
        #   которого подобрал бонусные баллы (BonusType.PURE_SCORE)

        self.washer_damage = washer_damage  # =0.15
        self.washer_initial_speed = washer_initial_speed  # =60.0
        self.washer_mass = washer_mass  # =10.0
        self.washer_radius = washer_radius  # =20.0
        self.side_washer_angle = side_washer_angle  # =0.03490658503988659

        self.nitro_duration_ticks = nitro_duration_ticks  # =120
        self.nitro_engine_power_factor = nitro_engine_power_factor  # =2.0
        self.use_nitro_cooldown_ticks = use_nitro_cooldown_ticks  # =120

        self.oil_slick_initial_range = oil_slick_initial_range  # =10.0
        self.oil_slick_lifetime = oil_slick_lifetime  # =600
        self.oil_slick_radius = oil_slick_radius  # =150.0
        self.spill_oil_cooldown_ticks = spill_oil_cooldown_ticks  # =120
        self.max_oiled_state_duration_ticks = max_oiled_state_duration_ticks  # =60

        self.tick_count = tick_count  # =3720
        self.tire_damage_factor = tire_damage_factor  # =0.35
        self.tire_disappear_speed_factor = tire_disappear_speed_factor  # =0.25
        self.tire_initial_speed = tire_initial_speed  # =60.0
        self.tire_mass = tire_mass  # =1000.0
        self.tire_radius = tire_radius  # =70.0
        self.throw_projectile_cooldown_ticks = throw_projectile_cooldown_ticks  # =60
        #   Возвращает длительность задержки в тиках, применяемой к кодемобилю после метания им снаряда.
        #   В течение этого времени кодемобиль не может метать новые снаряды

        self.buggy_engine_forward_power = buggy_engine_forward_power  # =312.5
        self.buggy_engine_rear_power = buggy_engine_rear_power  # =234.375
        self.buggy_mass = buggy_mass  # =1250.0
        self.jeep_engine_forward_power = jeep_engine_forward_power  # =375.0
        self.jeep_engine_rear_power = jeep_engine_rear_power  # =281.25
        self.jeep_mass = jeep_mass  # =1500.0

        self.burning_time_duration_factor = burning_time_duration_factor  # =0.4 -
        #   Возвращает коэффициент, определяющий количество тиков до завершения игры после финиширования
        #   трассы очередным кодемобилем. Для получения более подробной информациисмотрите документацию к
        #   world.lastTickIndex

        self.car_angular_speed_factor = car_angular_speed_factor  # =0.0017453292519943296
        # Возвращает коэффициент, используемый для вычисления составляющей угловой
        # скорости кодемобиля, порождаемой движением кодемобиля при ненулевом относительном угле
        # поворота колёс. Для получения более подробной информации смотрите документацию к
        # move.wheelTurn
        self.car_crosswise_movement_friction_factor = car_crosswise_movement_friction_factor  # =0.25
        self.car_damage_score_factor = car_damage_score_factor  # =100.0
        self.car_elimination_score = car_elimination_score  # =100
        self.car_engine_power_change_per_tick = car_engine_power_change_per_tick  # =0.025
        self.car_width = car_width      # =210.0
        self.car_height = car_height    # =140.0
        self.car_lengthwise_movement_friction_factor = car_lengthwise_movement_friction_factor  # =0.001
        self.car_movement_air_friction_factor = car_movement_air_friction_factor  # =0.0075
        self.car_reactivation_time_ticks = car_reactivation_time_ticks  # =300
        self.car_rotation_air_friction_factor = car_rotation_air_friction_factor  # =0.0075
        self.car_rotation_friction_factor = car_rotation_friction_factor  # =0.008726646259971648
        self.car_wheel_turn_change_per_tick = car_wheel_turn_change_per_tick  # =0.05

        self.finish_lap_score = finish_lap_score  # =1000
        self.finish_track_scores = finish_track_scores  # =[512, 256, 128, 64]
        self.initial_freeze_duration_ticks = initial_freeze_duration_ticks  # =180
        self.lap_count = lap_count  # =2
        self.lap_tick_count = lap_tick_count  # =1770
        self.lap_waypoints_summary_score_factor = lap_waypoints_summary_score_factor  # =0.5

        self.random_seed = random_seed  # =1245196268489704794

        self.track_tile_margin = track_tile_margin  # =80.0
        #   Возвращает отступ от границы тайла до границы прямого участка трассы,
        #   проходящего через этот тайл. Радиусы всех закруглённых сочленений участков трассы
        #   также равны этому значению
        self.track_tile_size = track_tile_size  # =800.0
        #   Возвращает размер (ширину и высоту) одного тайла

        self.world_height = world_height  # =8
        self.world_width = world_width  # =8