# coding: utf8
from math import *
from model.Bonus import Bonus
from model.Car import Car
from model.Direction import Direction
from model.OilSlick import OilSlick
from model.Player import Player
from model.Projectile import Projectile
from model.TileType import TileType


class World:
    def __init__(self, tick, tick_count, last_tick_index, width, height, players, cars, projectiles, bonuses,
                 oil_slicks, map_name, tiles_x_y, waypoints, starting_direction):
        self.tick = tick  # текуший тик
        self.tick_count = tick_count # ~=3720
        self.last_tick_index = last_tick_index  # ~=3719
        self.width = width  # =8 for default map
        self.height = height # =8 for default map
        self.players = players # [ <model.Player.Player instance at 0x7f72050d0a28> * 4 ]
        self.cars = cars  # [ <model.Car.Car instance at 0x7f72050d05f0> * 4]
        self.projectiles = projectiles  # =[] often
        self.bonuses = bonuses  # [ <model.Bonus.Bonus instance at 0x7f72050d0710> * N ]
        self.oil_slicks = oil_slicks # =[] often
        self.map_name = map_name  # =default
        self.tiles_x_y = tiles_x_y
        #    0  1  2  3  4  5  6  7
        # 0 [3, 1, 1, 1, 1, 1, 1, 5], default
        # 1 [2, 0, 0, 0, 0, 0, 0, 2],
        # 2 [2, 0, 0, 0, 0, 0, 0, 2],
        # 3 [2, 0, 0, 0, 0, 0, 0, 2],
        # 4 [2, 0, 0, 0, 0, 0, 0, 2],
        # 5 [2, 0, 0, 0, 0, 0, 0, 2],
        # 6 [2, 0, 0, 0, 0, 0, 0, 2],
        # 7 [4, 1, 1, 1, 1, 1, 1, 6]

        self.waypoints = waypoints
        # [[0, 6], [0, 1], [0, 0], [1, 0], [6, 0], [7, 0],
        #  [7, 1], [7, 6], [7, 7], [6, 7], [1, 7], [0, 7] ],

        self.starting_direction = starting_direction  # =2(up) often

    def get_my_player(self):
        for player in self.players:
            if player.me:
                return player

        return None