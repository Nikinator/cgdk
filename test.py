#coding:utf8
class Node(object):
    get_property = lambda n: property(
        lambda s: getattr(s, n),
        lambda s, v: setattr(s, n, v),
        lambda s: delattr(s, n))

    def __init__(self, *_val):
        self._val, self._parent, self._childs = _val, None, []

    @property
    def childs(self): return self._childs
    @childs.setter
    def childs(self, value):
        assert isinstance(value, list), 'Must be list!'
        self._childs = value
        for c in self._childs: c.parent = self
    @childs.deleter
    def childs(self): del self._childs

    @property
    def parent(self): return self._parent
    @parent.setter
    def parent(self, value):
        assert isinstance(value, self.__class__), 'Must be Node!'
        self._parent = value
    @parent.deleter
    def parent(self): del self._parent

    @property
    def val(self): return self._val if len(self._val) > 1 else self._val[0]
    @val.setter
    def val(self, *value): self._val = value

    is_leaf = property(lambda s: not len(s._childs))

    def __str__(self): return "Node('{}', '{}')".format(
        self.val, 'root' if self.parent is None else (
            'leaf' if self.is_leaf else 'node'))
    def __repr__(self): return str(self)

    def print_tree(self):
        """печатает узел и его потоков в виде дерева"""
        print '    ' * (self.length - 1) + str(self)
        for ch in self._childs:
            ch.print_tree()

    @property
    def length(self):
        # print self.val[1]
        return 1 if self._parent is None else (self._parent.length + 1)

    def get_branch(self, get_val=None):
        val = get_val(self.val) if get_val else self.val
        if self.parent:
            res = self.parent.get_branch(get_val)
            res.append(val)
            return res
        return [val]

    def is_contain(self, val, func_val=None):
        if isinstance(val, self.__class__): val = val.val
        is_found = (func_val(self._val) if func_val else self.val) == val
        if not is_found and self._parent:
            is_found = self._parent.is_contain(val, func_val)
        return is_found

# root = Node('a')
# b, e = Node('b'), Node('e')
# root.childs = [b, e]

# c, d = Node('c'), Node('d')
# b.childs = [c, d]
# f, g, h = Node('f'), Node('g'), Node('h')
# e.childs = [f, g, h]

# # root.childs[0].parent.val = 'a'
# print root.childs[0].parent, f, g, h.get_branch()
# # root.print_tree()
# print g.is_contain(c)


class D(object):
    L, R, U, D = 'l', 'r', 'u', 'd'
    LEFT, RIGHT, UP, DOWN = L, R, U, D
    _revers = {L: R, R: L, U: D, D: U}
    @classmethod
    def get_rev(cls, dir): return cls._revers[dir]

    @classmethod
    def get_next_point(cls, p, direct):
        x, y = p
        if direct == cls.UP: y -= 1
        elif direct == cls.DOWN: y += 1
        elif direct == cls.LEFT: x -= 1
        elif direct == cls.RIGHT: x += 1
        else: assert 0, u"Указано не существующее направление '{}'".format(direct)
        return (x, y)

class TT(object):
    U, E, V, H, C = tuple('uevhc')
    LTC, RTC, LBC, RBC = 'ltc', 'rtc', 'lbc', 'rbc'
    LT, RT, TT, BT = 'lt', 'rt', 'tt', 'bt'

    _directions = {
        E: [],                          # ▢
        V: [D.UP, D.DOWN],                  # | ║
        H: [D.LEFT, D.RIGHT],               # ⎻ ═
        LTC: {D.LEFT: [D.DOWN], D.UP: [D.RIGHT]},   # ╔
        RTC: {D.RIGHT: [D.DOWN], D.UP: [D.LEFT]},   #  ╗
        LBC: {D.LEFT: [D.UP], D.DOWN: [D.RIGHT]},   # ╚
        RBC: {D.RIGHT: [D.UP], D.DOWN: [D.LEFT]},   #  ╝
        LT: [D.LEFT, D.UP, D.DOWN],         # ⊣  ╣
        RT: [D.RIGHT, D.UP, D.DOWN],        # Ꮀ ╠
        TT: [D.UP, D.LEFT, D.RIGHT],        # ᚆ  ╩
        BT: [D.DOWN, D.LEFT, D.RIGHT],      # T ╦
        C: [D.UP, D.DOWN, D.LEFT, D.RIGHT], # +  ╬
        U: [],
    }

    _not_direct_in = "У типа тайла '{}' нет входящего направления '{}'."
    @classmethod
    def get_directs_out(cls, tile_type, direct_in):
        """print TT.get_outs(TT.LBC, D.LEFT)"""
        outs = cls._directions[tile_type]

        if outs:
            if isinstance(outs, list):
                assert D.get_rev(direct_in) in outs, (
                    cls._not_direct_in.format(tile_type, direct_in))
                outs = filter(lambda d: d != D.get_rev(direct_in), outs)
            elif isinstance(outs, dict):
                assert direct_in in outs, (
                    cls._not_direct_in.format(tile_type, direct_in))
                outs = outs[direct_in]
        return outs

# print TT.get_directs_out(TT.RT, D.DOWN)
# print D.get_next_point_by_direct((0,0),D.L)
_map = [ #06
    ['ltc', 'h', 'rtc', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'rtc', 'rt', 'rtc', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'rt', 'lt', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'v', 'v', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'rt', 'lt', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'],
['rt', 'lt', 'rt', 'rbc', 'e', 'e', 'e', 'ltc', 'h', 'rtc', 'e', 'e', 'e', 'ltc', 'bt', 'rtc'],
['rt', 'lt', 'v', 'ltc', 'bt', 'bt', 'bt', 'lt', 'e', 'rt', 'bt', 'bt', 'bt', 'lt', 'v', 'v'],
['rt', 'lt', 'lbc', 'tt', 'tt', 'tt', 'rbc', 'lbc', 'h', 'tt', 'tt', 'tt', 'tt', 'rbc', 'rt', 'rbc'],
['lbc', 'tt', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'rbc', 'e'],
]
get_type = lambda x,y: _map[y][x]
include = lambda x0,x,x1: min(x0,x1) <= x <= max(x0,x1)
def bad_point(p0, p, di, p1):
    # если не можем зайти в тайл - плохая точка
    try: TT.get_directs_out(get_type(*p), di)
    except AssertionError, e: return True  # e

    (x0, y0), (x, y), (x1, y1) = p0, p, p1
    dx, dy = abs(x1 - x0), abs(y1 - y0)

    # if dx == dy:
    #     # если хоть одна координата не лежит между p0,p1 - плохая точка
    if not (include(x0, x, x1) or include(y0, y, y1)): return True
    # else:
    #     # если только обе координаты не лежат между p0,p1 - плохая точка
    if not (include(x0, x, x1) and include(y0, y, y1)): return True

    # если оказались здесь - нормальная точка
    return False


def get_end_node(node, p1):
    p0, di = node.val
    if not isinstance(p1, tuple): p1 = tuple(p1)
    # уже добрались до желаемой точки - возвращаем узел
    if p0 == p1: return node

    # возможные выходы из тайла при указанном входе
    dirs_out = TT.get_directs_out(get_type(*p0), di)

    # отбирем только "хорошие" перемещения и
    # устанавливаем их дочерними узлами для node
    good_ways = []
    for do in dirs_out:
        p_n, di_n = D.get_next_point(p0, do), do
        # print bad_point(p0, p_n, di_n, p1),p0,di, p_n, di_n, p1
        if bad_point(p0, p_n, di_n, p1) or node.is_contain(p_n, get_first) : continue
        good_ways.append((p_n, di_n))
    node.childs = [Node(way) for way in good_ways]

    # print node.childs
    # Рекурсивно(в глубину) обходим дочение узлы,
    # получив списов возможных листьевых узлов,
    # которые позволили добраться до точки p1.
    end_nodes = filter(None, [
        get_end_node(child, p1) for child in node.childs])

    # Далее лишь возвращаем кратчайший из них или
    # None - не получилось добраться конечной точки.
    if not end_nodes:
        end_node = None
    elif len(end_nodes) == 1:
        end_node = end_nodes[0]
    else:
        # в списке будет всего 2-3 элемента, поэтому можно
        # в качестве ключа для сортировки указать .length
        end_node = sorted(end_nodes, key=lambda n: n.length)[0]

    return end_node

wp = [[13, 15], [1, 15], [0, 0], [2, 0], [2, 14],    (8,12),   [13, 13] ,(14,12)]
wp = map(tuple, wp)
# print TT.get_directs_out('rt', 'd')
# print D.get_next_point(wp[2], 'r')
# print bad_point((6, 3), (6, 4), 'r', (6, 6))
get_first = lambda v: v[0]
# node = get_end_node(Node((8,12), 'r'), (13,13))  # u???
# print node.get_branch(get_first) if node else node

p0, di, track = wp.pop(0), 'l', []
wp.append(p0)
for p in wp:
    node = get_end_node(Node(p0, di), p)
    if node:
        # print node, node.get_branch()[1:], node.length
        track.extend(node.get_branch(get_first)[:-1])
    else:
        print p0, di, p, 11111111111
    p0, di = node.val
print track
[(13, 15), (12, 15), (11, 15), (10, 15), (9, 15),
(8, 15), (7, 15), (6, 15), (5, 15), (4, 15), (3, 15),
(2, 15), (1, 15), (0, 15), (0, 14), (0, 13), (0, 12),
(0, 11), (0, 10), (0, 9), (0, 8), (0, 7), (0, 6),
(0, 5), (0, 4), (0, 3), (0, 2), (0, 1),
(0, 0), (1, 0), (2, 0), (2, 1), (2, 2), (2, 3),
(2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9),
(2, 10), (2, 11), (2, 12), (2, 13)]